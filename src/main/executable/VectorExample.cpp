/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <mathStruct/vector/Vector.h>
#include <mathStruct/vector/TVector.hxx>
#include <mathStruct/vector/VectorAlgebraic.hxx>

#include <iostream>
#include <dataStruct/DoubleArray.h>
#include <alg/Alg.h>
#include <utils/Timer.hxx>
#include <dataStruct/Array.hxx>
#include <utils/Random.hxx>

using std::cin;
using std::cout;

int main() {

//    M b;
//    std::cout<<(++b++).getT();
//    M::d c = b++;
//    cw::Vector vector;
//    std::cin>>vector;
//    std::cout<<vector;
//    cout<<(cw::Vector(10,10)==cw::Vector(10,10));
//    double asd[3]= {0,1,2};
//    double asd1[3]= {1,1,1};
//    cout<<cw::Vector(asd,3).arraySize()<<'\n';
//    cout<<cw::Vector(asd1,3).arraySize()<<'\n';
//    cout<<cw::Vector(asd,3).angle(cw::Vector(asd1,3))<<'\n';
//    cin>>vector;
//    cout<<vector;


//    cout<<cw::Vector::getCountObjects();
//    cout<<(-cw::Vector(asd,5));/
//    cout<<(cw::Vector(a,5)==cw::Vector(a,5));
//    std::cout<<vector;
//    std::cout<<++(++vector);
//    std::cout<<vector++;
//    std::cout<<++(-vector);


//    cw::TVector<double> v1(10,1.1);
//    cout<<v1;
//    cw::VectorAlgebraic v2;
//    cin>>v2;
//    cout<<v2;
//    bool flag = false;
//    int a=10,b=11;
//    bool f = a > b;
//    bool t = a < b;
//    bool b1 = f | flag;
//    bool b2 = f ^ flag;
//    bool b3 = f & flag;
//    bool b_1 = t | flag;
//    bool b_2 = t ^ flag;
//    bool b_3 = t & flag;
////    bool b1 = b2  flag;
//    cout << b1 <<' ';
//    cout << b2 <<' ';
//    cout << b3 <<'\n';
//    cout << b_1 <<' ';
//    cout << b_2 <<' ';
//    cout << b_3 <<'\n';


//    DoubleArray array(10);
//    array.RandArray(10);
//    for (size_t i = 0; i < array.Count(); ++i) {
//        cout<<array[i]<<',';
//    }
//    const Timer::StartedTimer &timer = Timer::start();
//    cw::sort::bubbleSort(array,true);
//    std::cout<<'\n'<<timer.finish();
//    cout<<'\n';
//    for (size_t i = 0; i < array.Count(); ++i) {
//        cout<<array[i]<<',';
//    }
    const Timer::StartedTimer &timer1 = Timer::start();
    std::cout << '\n' << timer1.finish() << std::endl;

//    cw::Array<double>::random(1).arraySize();
//    for (int j = 0; j < 1000; ++j) {
//        std::cout<< cw::random::rand<uint64_t>()<<std::endl;
//
//    }
    const Timer::StartedTimer &timer2 = Timer::start();
    const cw::Array<long> &array1 = cw::Array<long>::random(100);
    std::cout << '\n' << timer2.finish() << std::endl;
    cout << array1;
//    cw::sort::bubbleSort(array1);
    long time = array1.sort(cw::sort::bubbleSort<cw::Array<long>>);
    std::cout << array1;
    std::cout << time << std::endl;
    for (size_t i = 1; i < 100; ++i) {
        size_t size = 1000 * i;
        const cw::Array<long> &array = cw::Array<long>::random(size);
        long timec = array.sort(cw::sort::bubbleSort<cw::Array<long>>);
        std::cout << "Time(nano seconds):" << timec << std::endl;
        std::cout << "Time(seconds):" << timec * 1e-9 << std::endl;
        std::cout << "Size:" << size << std::endl << std::endl;
    }

    return 0;
}
