/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <mathStruct/matrix/Matrix.hxx>
#include <mathStruct/matrix/MatrixRectangle.hxx>
#include <mathStruct/matrix/MatrixSquare.hxx>
#include <iostream>
#include <utils/Timer.hxx>

int main() {
    const cw::TickTimer::StartedTickTimer &timer = cw::TickTimer::start();
    for (int j = 0; j < 1000; ++j) {
        uint64_t i = cw::rdtsc();
        uint64_t i1 = cw::rdtsc();
        std::cout << i1 - i << ' ';
    }
//    cw::Matrix matrix(10, 10,1.);
//
//    std::cout << matrix;

//    cw::MatrixRectangle matrixRectangle(2, 2);
//    std::cin>>matrixRectangle;
//    std::cout<<matrixRectangle;
    cw::MatrixSquare<double> square(2, 1.3);
    square(0, 0) = 2;
    square(0, 1) = 1;
    square(1, 0) = 6;
    square(1, 1) = 2.2;
//    square(4,3) = 6;
    std::cout << square << '\n';
    std::cout << square.determinant() << '\n';
    std::cout << timer << '\n';
    std::cout << timer.finish() << '\n';
    std::cout << timer.started() << '\n';
    return 0;
}