/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>

using namespace std;

#include <math.h>

double *solve(double **matrix, int cols, double eps) {
    int size = cols - 1;
    double *x0 = new double[size];
    for (int i = 0; i < cols; i++) x0[i] = 0;
    int e = 0;
    while (true) {
        e++;
        double *x1 = new double[size];
        for (int i = 0; i < size; i++) {
            double res = matrix[i][size];
            for (int j = 0; j < size; j++) {
                res -= (i != j) ? matrix[i][j] * x0[j] : 0;
            }
            x1[i] = res / matrix[i][i];
        }
        bool flag = false;
        for (int i = 0; i < cols; i++) {
            if (fabs(x1[i] - x0[i]) > eps) flag = true;
        }
        delete[] x0;
        x0 = x1;
        if (!flag)break;
    }
    return x0;
}

int main() {
    auto **matrix = new double *[3];
    matrix[0] = new double[4];
    matrix[1] = new double[4];
    matrix[2] = new double[4];
    matrix[0][0] = 8;
    matrix[0][1] = 0;
    matrix[0][2] = -12;
    matrix[0][3] = 58;
    matrix[1][0] = 0;
    matrix[1][1] = 51;
    matrix[1][2] = 12;
    matrix[1][3] = -41;
    matrix[2][0] = -12;
    matrix[2][1] = 12;
    matrix[2][2] = 24;
    matrix[2][3] = -88;
    double *result = solve(matrix, 4, 1e-5);
    for (int i = 0; i < 2; ++i) {
        cout << result[i] << endl;
    }
    return 0;
}