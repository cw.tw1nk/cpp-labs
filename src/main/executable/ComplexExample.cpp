/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <mathStruct/complex/Complex.h>

using namespace std;

int main() {
    cw::Complex complex(3, 6);
    cw::Complex complex2(5, 2);
    cout << complex;
    cout << "\n";
    cout << complex2;
    cout << "\n";
//    cout<<Complex::root(-16,3)[0];
//    cout<<"\n";
//    cout<<Complex::root(-16,3)[1];
//    cout<<"\n";
//    cout<<Complex::root(-16,3)[2];
//    cout<<"\n";
//    cout<<Complex::root(-16,3)[3];
//    cout<<"\n";
//    cout<<"\n";
////    cout<<"rt()="<<rt(8,3);
////    cout<<Complex::root(-4,2)[1];
//    cout<<"\n";
//    cout<<complex.root(2)[0];
//    cout<<"\n";
//    cout<<complex.root(2)[1];
    cout << complex + complex2;
//    cout<<complex.add(complex2)<<"\n"<< complex.substract(complex2) << "\n"<< complex.multiply(complex2)<<"\n"<< complex.devide(complex2);
//    cout<<complex + complex2<<"\n"<< complex - complex2 << "\n"<< complex*complex2<<"\n"<< complex/complex2<<"\n";
//    cout<<"\n";
//    cout<<1.5+complex;
//    cout<<complex+1.5;
//    cout<<"\n";
//    cout<<complex-1.5;
//    cout<<1.5-complex;
//    cout<<"\n";
//    cout<<complex*1.5;
//    cout<<1.5*complex;
//    cout<<"\n";
//    cout<<complex/1.5;
//    cout<<1.5/complex;
//    cout<<complex.module();
//    cout<<"\n";
//    cout<<complex.angle();
//    cout<<"\n";
//    cout<<"z = "<<complex.module()<<"("<<cos(complex.angle())<<"+i"<<sin(complex.angle());
//    cout<<"\n";
//    cout<< complex.module()*cos(complex.angle())<<complex.module()*sin(complex.angle());
//    cout<<"\n";
//    cout<<"binPow() = " <<binPow(2.5999192399123,2);
//    cout<<"\n";
//    cout<<rt(4,2);
    cout << "\n" << complex.Ln(0);
    cout << "\n" << (cw::Complex(1, 2) ^ cw::Complex(3, 4));
    cout << '\n' << cw::Complex(1, 2).angle();

    return 0;
}
