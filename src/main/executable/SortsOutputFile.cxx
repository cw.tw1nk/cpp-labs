/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <fstream>
#include <dataStruct/TArray.hxx>
#include <alg/Alg.h>
#include <dataStruct/DoubleArray.h>
#include <utils/lcg_rand.h>
#include <utils/Timer.hxx>

using namespace std;

int main() {

    std::cout << "Start bubble\n";

    ofstream os;
    os.open("sorts/BubbleSort.txt");
    os << "====================================================\n";
    os << "====================Bubble sort=====================\n";
    os << "====================================================\n";
    int size = 32768;
    cw::TArray<int> array = cw::TArray<int>::seq(size);
    cw::random::lcg::generateSeed();
    array.shuffle(cw::random::lcg::rand);
    os << array << '\n';
    const cw::Timer::StartedTimer &timer = cw::Timer::start();
    cw::sort::bubbleSort<cw::TArray<int>>(array);
    int64_t time = timer.finish();
    os << "====================================================\n";
    os << "====================Sorted Array====================\n";
    os << "====================================================\n";
    os << array << "\n";
    os << "====================================================\n";
    os << "========================Info========================\n";
    os << "====================================================\n";
    os << "====================" << "Array size: " << array.size() << "====================\n";
    os << "====================" << "Sort time: " << time << "====================\n";
    os.close();
    std::cout << "End bubble sort\n";


    std::cout << "Start insertion sort\n";
    os.open("sorts/InsertionSort.txt");
    os << "====================================================\n";
    os << "===================Insertion sort===================\n";
    os << "====================================================\n";
    array.shuffle(cw::random::lcg::rand);
    os << array << '\n';
    const cw::Timer::StartedTimer &timer2 = cw::Timer::start();
    cw::sort::insertionSort<cw::TArray<int>>(array);
    time = timer2.finish();
    os << "====================================================\n";
    os << "====================Sorted Array====================\n";
    os << "====================================================\n";
    os << array << "\n";
    os << "====================================================\n";
    os << "========================Info========================\n";
    os << "====================================================\n";
    os << "====================" << "Array size: " << array.size() << "====================\n";
    os << "====================" << "Sort time: " << time << "====================\n";
    os.close();
    std::cout << "End insertion sort\n";


    std::cout << "Start selection sort\n";
    os.open("sorts/SelectionSort.txt");
    os << "====================================================\n";
    os << "================SelectionSort sort==================\n";
    os << "====================================================\n";
    array.shuffle(cw::random::lcg::rand);
    os << array << '\n';
    const cw::Timer::StartedTimer &timer3 = cw::Timer::start();
    cw::sort::selectionSort<cw::TArray<int>>(array);
    time = timer3.finish();
    os << "====================================================\n";
    os << "====================Sorted Array====================\n";
    os << "====================================================\n";
    os << array << "\n";
    os << "====================================================\n";
    os << "========================Info========================\n";
    os << "====================================================\n";
    os << "====================" << "Array size: " << array.size() << "====================\n";
    os << "====================" << "Sort time: " << time << "====================\n";
    os.close();
    std::cout << "End selection sort\n";
}