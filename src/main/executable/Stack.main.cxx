/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <dataStruct/Stack.hxx>

int main() {
    using std::cout, std::endl;
    cw::Stack<int> stack;
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);
    stack.push(5);
    std::cout << stack << endl;
    for (size_t i = 0; i < 5; ++i) {
        cout << "Delete element: " << stack.pop() << endl;
    }
    if (stack.isEmpty()) {
        std::cerr << "Stack is empty!" << endl;
    }
    std::cout << stack;
    stack.push(10);
    stack.push(11);
    stack.push(12);
    stack.push(13);
    stack.push(14);

    std::cout << "Peek elem: " << stack.peek() << endl;
    std::cout << "Peek elem: " << stack.peek() << endl;
    std::cout << "Full array: " << stack;

}
