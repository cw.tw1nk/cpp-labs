/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <dataStruct/TArray.hxx>
#include <alg/Alg.h>
#include <dataStruct/DoubleArray.h>
#include <utils/lcg_rand.h>
#include <utils/Timer.hxx>

int main() {
    int size = 500;
    cw::TArray<int> array = cw::TArray<int>::seq(size);
    cw::random::lcg::generateSeed();
    array.shuffle(cw::random::lcg::rand);
//        std::cout << "Array arraySize: " << array.size() << '\n';

    //    std::cout<<array<<'\n';
    const cw::Timer::StartedTimer &timer = cw::Timer::start();
    cw::sort::parallelQuickSort<cw::TArray<int>, int>(array, 0, static_cast<int>(array.size() - 1),
                                                      cw::sort::helper::compare<int>);
    int64_t time = timer.finish();

    std::cout << time << std::endl;
    std::cout << array << std::endl;

}