/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <dataStruct/TArray.hxx>
#include <alg/Alg.h>

int main() {
    const cw::TArray<int> &array = cw::TArray<int>::seq(20);
    array.shuffle();
    std::cout << array << "\n\n\n";
    cw::sort::margeSort(array, 0, array.size() - 1, cw::sort::helper::compare<int>);
    std::cout << array;
    return 0;
}
