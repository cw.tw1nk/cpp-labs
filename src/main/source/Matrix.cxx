/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CPP_OPP_MATRIXT_CPP
#define CPP_OPP_MATRIXT_CPP


#include <mathStruct/matrix/Matrix.hxx>
#include <stdexcept>
#include <sstream>
#include <cmath>
#include <cstring>

namespace cw {

    template<class T>
    Matrix<T>::Matrix(): rows(0), columns(0) {
        rows = 0;
        columns = 0;
        array = nullptr;
    }

    /**
     * @throw std::bad_alloc When memory allocation is failed
     * @tparam T
     * @param rows
     * @param columns
     */
    template<class T>
    Matrix<T>::Matrix(size_t rows, size_t columns): rows(rows), columns(columns) {
        try {
            array = Matrix<T>::createArray2d(rows, columns);
        } catch (std::bad_alloc &e) {
            throw e;
        }
    }

    /**
     * @throw std::bad_alloc When memory allocation is failed
     * @tparam T
     * @param Rows
     * @param Columns
     * @param object
     */
    template<class T>
    Matrix<T>::Matrix(size_t Rows, size_t Columns, T object): rows(Rows), columns(Columns) {
        try {
            array = Matrix<T>::createArray2d(Rows, Columns);
        } catch (std::bad_alloc &e) {
            throw e;
        }
        for (size_t i = 0; i < Rows; ++i) {
            for (size_t j = 0; j < Columns; ++j) {
                array[i][j] = object;
            }
        }
    }

    template<class T>
    Matrix<T>::Matrix(const Matrix<T> &other): rows(other.rows), columns(other.columns) {
        if (rows == 0 or columns == 0) {
            array = nullptr;
        } else {
            try {
                array = Matrix<T>::createArray2d(rows, columns);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            memcpy(array[0], other.array[0], sizeof(T) * rows * columns);
            for (size_t i = 1; i < rows; ++i) {
                *(array + i) = *array + columns * i;
            }
        }
    }

    /**
     * @throw std::out_of_range When index is out of range or negative
     * @tparam T
     * @param row
     * @param column
     * @return
     */
    template<class T>
    T &Matrix<T>::operator()(long row, long column) const {
        if (row < 0 or column < 0) {
            throw std::out_of_range("Index can not be negative");
        } else if ((size_t) row >= rows or (size_t) column >= columns) {
            throw std::out_of_range("Index out of range");
        }
        return array[row][column];
    }

    template<class T>
    Matrix<T>::~Matrix() {
        delete[] array;
    }

    template<class T>
    const bool Matrix<T>::operator==(const Matrix<T> &matrixT) const {
        if (rows != matrixT.rows or columns != matrixT.columns) {
            return false;
        } else {
            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < columns; ++j) {
                    if (array[i][j] != matrixT.array[i][j]) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    template<class T>
    const bool Matrix<T>::operator!=(const Matrix<T> &matrixT) const {
        if (rows != matrixT.rows or columns != matrixT.columns) {
            return true;
        } else {
            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < columns; ++j) {
                    if (array[i][j] != matrixT.array[i][j]) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * @throw std::bad_alloc When memory allocation is failed
     * @tparam T
     * @param other
     * @return
     */
    template<class T>
    Matrix<T> &Matrix<T>::operator=(const Matrix<T> &other) {
        if (*this == other) {
            return *this;
        } else if (rows == other.rows and columns == other.columns) {
            memcpy(array[0], other.array[0], sizeof(T) * rows * columns);
            for (size_t i = 1; i < rows; ++i) {
                *(array + i) = *array + columns * i;
            }
        } else {
            delete[] array;
            rows = other.rows;
            columns = other.columns;
            try {
                array = Matrix<T>::createArray2d(rows, columns);
            } catch (std::bad_alloc &e) {
                string ErrorMessage(e.what());
                ErrorMessage += "\n called by operator =\n";
                throw std::bad_alloc(ErrorMessage.c_str());
            }
            memcpy(array[0], other.array[0], sizeof(T) * rows * columns);
            for (size_t i = 1; i < rows; ++i) {
                *(array + i) = *array + columns * i;
            }

        }
        return *this;
    }

    template<class T>
    std::ostream &operator<<(std::ostream &os, const Matrix<T> &t) {
        for (size_t i = 0; i < t.rows; ++i) {
            for (size_t j = 0; j < t.columns; ++j) {
                os << t.array[i][j] << ' ';
            }
            os << std::endl;
        }
        os << std::endl;
        return os;
    }

    template<class T>
    std::istream &operator>>(std::istream &is, Matrix<T> &t) {
        std::string string1;
        std::getline(is, string1);                   //getting Size
        string1 = trim(string1);
        size_t RowQuantity = 1;
        if (string1.empty()) {
            if (t.getRows() != 0) {
                t.rows = 0;
                t.columns = 0;
                delete[] t.array;
            }
        } else {
            double temp;
            std::istringstream ss(string1);
            ss >> temp;
            if (fabs(fmod(temp, 1)) > 0) {
                throw std::domain_error("Row quantity can not be fractional");
            } else if (temp < 0) {
                throw std::domain_error("Row quantity can not be negative");
            } else {
                RowQuantity = (size_t) temp;
                if (RowQuantity != t.rows) {
                    t.rows = RowQuantity;
                }
            }
        }
        size_t ColumnsQuantity = 1;
        std::getline(is, string1);
        std::string tempString;
        std::unique_copy(string1.begin(), string1.end(),
                         std::back_inserter(tempString), // deleting repeating whitespaces
                         [](char c1, char c2) { return isspace(c1) and isspace(c2); });
        cout << tempString << endl;
        tempString = trim(
                tempString);                                         //deleting whitespaces at begin, and end of string
        if (tempString.empty()) {
            delete[] t.array;
            t.rows = 0;
            return is;
        }
        for (char i : tempString) {
            if (isspace(i)) {
                ++ColumnsQuantity;
            }
        }
        t.columns = ColumnsQuantity;
        delete[] t.array;
        t.array = t.createArray2d(t.rows, t.columns);
        std::istringstream ss(tempString);
        for (size_t j = 0; j < t.columns; ++j) {
            ss >> t.array[0][j];
        }
        string1.erase();
        tempString.erase();
        for (size_t k = 1; k < t.rows; ++k) {
            std::getline(is, string1);
            std::unique_copy(string1.begin(), string1.end(),
                             std::back_inserter(tempString), // deleting repeating whitespaces
                             [](char c1, char c2) { return isspace(c1) and isspace(c2); });
            tempString = trim(
                    tempString);                                         //deleting whitespaces at begin, and end of string
            if (tempString.empty()) {
                for (size_t j = 0; j < t.columns; ++j) {
                    t.array[k][j] = T();
                }
            }
            std::istringstream ss1(tempString);
            for (size_t j = 0; j < t.columns; ++j) {
                ss1 >> t.array[k][j];
            }
            string1.erase();
            tempString.erase();
        }
        return is;
    }

    template<class T>
    size_t Matrix<T>::getRows() const {
        return rows;
    }

    template<class T>
    size_t Matrix<T>::getColumns() const {
        return columns;
    }

    /**
     * @brief additional function to swap lines in matrix. Used in MatrixAlgebraic<T>::determinant()
     * @tparam T
     * @param[in] array Matrix to swap
     * @param[out] array Matrix with swaped lines
     * @param pos Which line swap
     * @param inPos To which position swap
     */
    template<class T>
    void Matrix<T>::swapLine(T **array, size_t src, size_t dest) const {
        T *temp = array[src];
        array[src] = array[dest];
        array[dest] = temp;
    }
}

#endif //CPP_OPP_MATRIXT_CPP