/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <sandbox/AAA.h>

I &BBB::operator+(I &i) {
    if (BBB *bbb = dynamic_cast<BBB *>(&i)) {
        std::cout << "bbb+bbb" << std::endl;
        this->test += bbb->test;
    } else if (AAA *aaa = dynamic_cast<AAA *>(&i)) {
        std::cout << "bbb+aaa" << std::endl;
        this->test += aaa->test;
    } else {
        return *this;
    }
    return *this;
}

int BBB::getValue() const {
    return test;
}

BBB::BBB(int test) : test(test) {}
