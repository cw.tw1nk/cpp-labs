/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef CPP_OOP_1_VECTOR_CPP
#define CPP_OOP_1_VECTOR_CPP

#include <mathStruct/vector/Vector.h>
#include <iostream>
#include <cstring>
#include <stdarg.h>
#include <regex>
#include <cmath>

namespace cw {
    size_t Vector::countObjects;

    /*
     * Constructors
     */
    Vector::Vector() : items(nullptr), size(0) {
        ++Vector::countObjects;
    }

    Vector::Vector(const Vector &vector) : size(vector.size) {
        items = new double[size];
        for (size_t i = 0; i < vector.size; ++i)
            items[i] = vector.items[i];
        ++Vector::countObjects;
    }


    Vector::Vector(size_t size) : items(new double[size]), size(size) {
        ++Vector::countObjects;
    }

    Vector::Vector(size_t size, double defaultValue) : size(size) {
        items = new double[size];
        for (size_t i = 0; i < size; ++i) {
            items[i] = defaultValue;
        }
        ++Vector::countObjects;
    }

    Vector::Vector(const double *array, size_t size_array) : size(size_array) {
        items = new double[size_array];
        for (size_t i = 0; i < size_array; i++) {
            items[i] = array[i];
        }
        ++Vector::countObjects;
    }

    Vector::~Vector() {
        delete[] items;
        --Vector::countObjects;

    }

    /*
     * binary operators
     */
    const Vector Vector::operator+(const Vector &v) const {

        if (size != v.size) {
            size_t smax = (size > v.size) ? size : v.size;
            size_t smin = (size > v.size) ? v.size : size;
            Vector vector(smax);
            for (size_t i = 0; i < smin; ++i) {
                vector.items[i] = items[i] + v.items[i];
            }
            if (size > v.size) {
                for (size_t i = smin; i < smax; ++i) {
                    vector.items[i] = items[i];
                }
            } else {
                for (size_t i = smin; i < smax; ++i) {
                    vector.items[i] = v.items[i];
                }
            }
            vector.size = smax;
            return vector;
        } else {
            if (v.size == 0) return *this;
            Vector vector(size);
            for (size_t i = 0; i < size; ++i) {
                vector.items[i] = items[i] + v.items[i];
            }
            vector.size = size;

            return vector;
        }
    }

    const Vector Vector::operator-(const Vector &v) const {
        return Vector(*this + (-v));
    }

    double Vector::operator*(const Vector &v) const {
        double res = 0;
        size_t smin = (size > v.size) ? v.size : size;
        for (size_t i = 0; i < smin; ++i) {
            res += items[i] * v.items[i];
        }
        return res;
    }

    const Vector Vector::operator/(double n) const {
        return *this * (1 / n);
    }

    bool Vector::operator==(const Vector &rhs) const {
        if (size != rhs.size) return false;
        else {
            for (size_t i = 0; i < size; ++i) {
                if (items[i] != rhs.items[i]) return false;
            }
        }
        return true;
    }

    bool Vector::operator!=(const Vector &rhs) const {
        return !(rhs == *this);
    }

    const Vector Vector::operator*(double n) const {
        Vector vector(*this);
        for (size_t i = 0; i < vector.size; ++i) {
            vector.items[i] *= n;
        }
        return vector;
    }

    Vector &Vector::operator=(const Vector &vector) {
        size = vector.size;
        if (items == nullptr) {
            items = new double[size];
        } else {
            delete[] items;
        }
        for (size_t i = 0; i < size; ++i) {
            items[i] = vector.items[i];
        }
        return *this;
    }

    const Vector &Vector::operator+=(const Vector &v) {
        if (v.size == 0) return *this;
        size_t smax = (size > v.size) ? size : v.size;
        size_t smin = (size > v.size) ? v.size : size;
        if (size < smax) {
            auto *localItems = new double[smax];
            for (size_t i = 0; i < smax; ++i) {
                localItems[i] = items[i];
            }
            delete[] items;
            items = localItems;
        }
        for (size_t i = 0; i < smin - 1; ++i) {
            items[i] += v.items[i];
        }
        if (size < v.size) {
            for (size_t i = smin; i < smax; ++i) {
                items[i] = v.items[i];
            }
        }
        return *this;
    }

    const Vector &Vector::operator-=(const Vector &v) {
        return *this += -v;
    }

    const Vector &Vector::operator*=(double z) {
        for (size_t i = 0; i < size; ++i) {
            items[i] *= z;
        }
        return *this;
    }

    const Vector &Vector::operator/=(double z) {
        return *this *= 1 / z;
    }

    double &Vector::operator[](size_t index) {
        if (index < size) throw std::out_of_range("Выход за пределы массива");
        return this->items[index];
    }

    /*
     * Static binary operators
     */
    std::ostream &operator<<(std::ostream &os, const Vector &vector) {
        size_t sz = vector.size;
        os << "\n{\n\t\"arraySize\": " << sz << "\n\t\"items\": [";
        for (size_t i = 0; i < sz; ++i) {
            os << "\n\t\t" << vector.items[i];
            if (i != sz - 1)os << ',';
        }
        os << "\n\t]\n}\n";
        return os;
    }

    std::istream &operator>>(std::istream &is, Vector &v) {
        std::string s;
        getline(is, s);
        lineToVector(s, v);
        return is;
    }

    /*
     * Unary operators
     */
    Vector::operator double *() const {
        auto *vector = new double[size];
        for (size_t i = 0; i < size; ++i) {
            vector[i] = items[i];
        }
        return vector;
    }

    Vector &Vector::operator++() {
        auto *temp = new double[size + 1];

        size_t size1 = size / 2;
        for (size_t i = 0; i < size1; ++i) {
            temp[i] = items[i];
            temp[i + size1] = items[i + size1];
        }
        delete[] items;
//        items = new double[arraySize+1];
        size += 1;
        items = temp;
        return *this;
    }


    const Vector Vector::operator++(int) {
        const Vector &vect = Vector(*this);
        auto *temp = new double[size + 1];

        size_t size1 = size / 2;
        for (size_t i = 0; i < size1; ++i) {
            temp[i] = items[i];
            temp[i + size1] = items[i + size1];
        }
        delete[] items;
        size += 1;
        items = temp;

        return vect;
    }

    Vector Vector::operator-() const {
        const Vector vector(*this);
        for (size_t i = 0; i < size; ++i) {
            vector.items[i] = -items[i];
        }
        return vector;
    }

    const Vector &Vector::operator+() const {
        return *this;
    }


//    const Vector Vector::seq(size_t arraySize, ...) {
//        va_list args;
//        va_start(args, arraySize);
//        Vector v;
//        while (arraySize--) {
//            v.add(va_arg(args, double));
//        }
//        va_end(args);
//        return v;
//    }


    const double Vector::len() const {
        double res = 0;
        for (size_t i = 0; i < size; ++i) {
            res += items[i] * items[i];
        }
        return std::sqrt(res);
    }

    const double Vector::angle(const Vector &&v) const {
        return acos((*this * v) / (len() * v.len()));
    }

//    const Vector &Vector::add(double item) {
//        if(items == nullptr) items = new double[BUFSIZ];
//        else if(arraySize==buffered_size){
//            delete [] items;
//            items = new double[buffered_size+=128];
//        }
//        items[arraySize] = item;
//        arraySize++;
//        return *this;
//    }

    const bool Vector::isEmpty() const {
        return size == 0;
    }

//
//    const Vector &Vector::clear() {
//        arraySize = 0;
//        return *this;
//    }

    void lineToVector(std::string s, Vector &v) {
        std::regex regexpr(R"((\d*\.?\d*)\s*\r*\n*)");
        std::cmatch result;
        std::regex_iterator<std::string::iterator> it(s.begin(), s.end(), regexpr);
        std::regex_iterator<std::string::iterator> end;
        delete[] v.items;
        long i1 = std::distance(it, end) - 1;
        v.items = new double[i1];
        for (size_t i = 0; it != end; ++it, ++i) {
            const auto &str = it->str();
            if (!str.empty())
                v.items[i] = (std::stod(str));
        }
        v.size = static_cast<size_t>(i1);
    }


}
#endif
