/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CPP_OOP_1_MEMORYDISPATCHER_HXX
#define CPP_OOP_1_MEMORYDISPATCHER_HXX

#include <unistd.h>

struct MemoryBlock {
    int is_available;
    int size;
};

class MemoryDispatcher {
    static MemoryDispatcher instance;
    void *managed_memory_start;
    void *last_valid_address;
private:
    void free(void *fb) {
        struct MemoryBlock *mcb;
        mcb = static_cast<MemoryBlock *>(fb - sizeof(struct mem_control_block));
        mcb->is_available = 1;
        return;
    }

    MemoryDispatcher() {
//        memoryBlocks= malloc(        1048576*256);
//        next = memoryAllocated;
        last_valid_address = sbrk(0);
        managed_memory_start = last_valid_address;

    }

    MemoryDispatcher(MemoryDispatcher &) = delete;

    void realloc(size_t newSize) {

    }

public:
    static MemoryDispatcher &getInstance() {
        return instance;
    }

};


#endif //CPP_OOP_1_MEMORYDISPATCHER_HXX
