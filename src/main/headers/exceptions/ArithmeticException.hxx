/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <exception>
#include <string>

#ifndef CPP_OOP_1_ARITHMETICEXCEPTION_HXX
#define CPP_OOP_1_ARITHMETICEXCEPTION_HXX
using std::string;
namespace cw {
    class ArithmeticException : public std::exception {
    private:
        const char *error;
    public:
        ArithmeticException(const char *error) : error(error) {}

        ArithmeticException() {
        }

    public:
        const char *what() const override {
            return error;
        }
    };
}

#endif //CPP_OOP_1_ARITHMETICEXCEPTION_HXX
