/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_VECTORALGEBRAICT_H
#define CPP_OOP_VECTORALGEBRAICT_H

#include <dataStruct/mathStruct/vector/TVector.hxx>
#include <regex>
#include <iostream>

namespace cw {
    template<class T = double>
    class VectorAlgebraic : public TVector<T> {
    public:

        VectorAlgebraic<T>();

        explicit VectorAlgebraic<T>(size_t Size);

        VectorAlgebraic<T>(size_t Size, T number);

        VectorAlgebraic<T>(const VectorAlgebraic<T> &vectorAlgebraicT);

        virtual ~VectorAlgebraic();

        const VectorAlgebraic<T> operator-() const;

        const VectorAlgebraic<T> operator+() const;

        const VectorAlgebraic<T> &operator++();

        const VectorAlgebraic<T> operator++(int);

        const VectorAlgebraic<T> &operator--();

        const VectorAlgebraic<T> operator--(int);

        const VectorAlgebraic operator+(const VectorAlgebraic<T> &vectorAlgebraicT) const;

        const VectorAlgebraic operator-(const VectorAlgebraic<T> &vectorAlgebraicT) const;

        T operator*(const VectorAlgebraic<T> &vectorAlgebraicT);

        const VectorAlgebraic<T> &operator+=(const VectorAlgebraic<T> &vectorAlgebraicT);

        const VectorAlgebraic<T> &operator-=(const VectorAlgebraic<T> &vectorAlgebraicT);

        const VectorAlgebraic<T> operator*(T number) const;

        const VectorAlgebraic<T> operator/(T number) const;

        template<class V>
        friend const VectorAlgebraic<V> operator*(V number, const VectorAlgebraic<V> &);

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const VectorAlgebraic<V> &vector);

        template<class V>
        friend std::istream &operator>>(std::istream &is, VectorAlgebraic<V> &t);


        T module() const;

    private:
        static void lineToVector(std::string s, VectorAlgebraic<T> &v);
    };

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic() = default;

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic(size_t Size):TVector<T>(Size) {
    }

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic(size_t Size, T number):TVector<T>(Size, number) {

    }

    template<class T>
    VectorAlgebraic<T>::VectorAlgebraic(const VectorAlgebraic<T> &vectorAlgebraicT):TVector<T>(vectorAlgebraicT) {
    }

    template<class T>
    VectorAlgebraic<T>::~VectorAlgebraic() = default;

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator-() const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic<T> result(*this);
        if (!this->Initialized) {
            for (size_t i = 0; i < this->Size; ++i) {
                result.Array[i] = -T();
            }
            result.Initialized = true;
        } else {
            for (size_t i = 0; i < this->Size; ++i) {
                result.Array[i] = -result.Array[i];
            }
        }
        return result;
    }

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator+() const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        return *this;
    }

    template<class T>
    const VectorAlgebraic<T> &VectorAlgebraic<T>::operator++() {
        this->Size += 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        temp[this->Size - 1] = T();
        delete[] this->Array;
        this->Array = temp;
        return *this;
    }

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator++(int) {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic<T> result(*this);
        this->Size += 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        temp[this->Size - 1] = T();
        delete[] this->Array;
        this->Array = temp;
        return result;
    }

    template<class T>
    const VectorAlgebraic<T> &VectorAlgebraic<T>::operator--() {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        this->Size -= 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        delete[] this->Array;
        this->Array = temp;
        return *this;
    }

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator--(int) {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic<T> result(*this);
        this->Size -= 1;
        T *temp = new T[this->Size];
        for (size_t i = 0; i < this->Size; ++i) {
            temp[i] = this->Array[i];
        }
        delete[] this->Array;
        this->Array = temp;
        return result;
    }


    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator+(const VectorAlgebraic<T> &vectorAlgebraicT) const {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        VectorAlgebraic<T> result;
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                result = VectorAlgebraic<T>(this->Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] + T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        result.Array[j] = T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        result.Array[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        result.Array[j] = this->Array[j] + T();
                    }
                }
            } else {                                                            //Справа больше чем слева
                result = VectorAlgebraic<T>(vectorAlgebraicT.Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] + T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() + vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = T();
                    }
                }
            }
        } else {                                                                    //Векторы имеют один размер
            result = VectorAlgebraic<T>(this->Size);
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] + T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    result.Array[i] = T() + vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                }
            }
        }
        result.Initialized = true;
        return result;
    }

    template<class T>
    T VectorAlgebraic<T>::module() const {
        if (this->Size == 0) {
            throw std::domain_error("Degenerated vector don't have module");
        }
        T module = T();
        if (!this->Initialized) {
            return sqrt(module);
        } else {
            for (size_t i = 0; i < this->Size; ++i) {
                module += this->Array[i] * this->Array[i];
            }
            return sqrt(module);
        }
    }

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator-(const VectorAlgebraic<T> &vectorAlgebraicT) const {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        VectorAlgebraic<T> result;
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                result = VectorAlgebraic<T>(this->Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] - T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        result.Array[j] = -T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        result.Array[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        result.Array[j] = this->Array[j] - T();
                    }
                }
            } else {                                                            //Справа больше чем слева
                result = VectorAlgebraic<T>(vectorAlgebraicT.Size);
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] - T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = -T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result.Array[i] = T() - vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result.Array[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result.Array[j] = -T();
                    }
                }
            }
        } else {                                                                    //Векторы имеют один размер
            result = VectorAlgebraic<T>(this->Size);
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] - T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    result.Array[i] = T() - vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    result.Array[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                }
            }
        }
        result.Initialized = true;
        return result;
    }

    template<class T>
    T VectorAlgebraic<T>::operator*(const VectorAlgebraic<T> &vectorAlgebraicT) {
        if (this->Size == 0 and vectorAlgebraicT.Size == 0) {
            throw std::domain_error("Vectors are degenerated");
        }
        T skalarMulti = T();
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Both is not initialized
            size_t temp = (this->Size > vectorAlgebraicT.size()) ? this->Size : vectorAlgebraicT.Size;
            for (size_t i = 0; i < temp; ++i) {
                skalarMulti += T() * T();
            }
        } else if (this->Size != vectorAlgebraicT.size()) {                    //Size don't macth
            if (this->Size > vectorAlgebraicT.Size) {                    //Left bigger than right
                if (this->Initialized and !vectorAlgebraicT.Initialized) {       //Lest initialized, right is not
                    for (size_t i = 0; i < this->Size; ++i) {
                        skalarMulti += this->Array[i] * T();
                    }
                } else if (!this->Initialized and
                           vectorAlgebraicT.Initialized) {//Left not initialized, Right is the opposite
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        skalarMulti += T() * vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        skalarMulti += T() * T();
                    }
                } else {                                                            //Both is initialized
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        skalarMulti += this->Array[i] * vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        skalarMulti += this->Array[j] * T();
                    }
                }
            } else {                                                        // Right is bigger than left
                if (this->Initialized and !vectorAlgebraicT.Initialized) { //Lest initialized, right is not
                    for (size_t i = 0; i < this->Size; ++i) {
                        skalarMulti += this->Array[i] * T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        skalarMulti += T() * T();
                    }
                } else if (!this->Initialized and
                           vectorAlgebraicT.Initialized) {//Left not initialized, Right is the opposite
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        skalarMulti += T() * vectorAlgebraicT.Array[i];
                    }
                } else {                                                      //Both is initialized
                    for (size_t i = 0; i < this->Size; ++i) {
                        skalarMulti += this->Array[i] * vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        skalarMulti += T() * vectorAlgebraicT.Array[j];
                    }
                }
            }
        } else {                                                    //Sizes are match
            if (this->Initialized and !vectorAlgebraicT.Initialized) {
                for (size_t i = 0; i < this->Size; ++i) {
                    skalarMulti += this->Array[i] * T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) {
                for (size_t i = 0; i < this->Size; ++i) {
                    skalarMulti += T() * vectorAlgebraicT.Array[i];
                }
            } else {
                for (size_t i = 0; i < this->Size; ++i) {
                    skalarMulti += this->Array[i] * vectorAlgebraicT.Array[i];
                }
            }
        }
        return skalarMulti;
    }

    template<class T>
    const VectorAlgebraic<T> &VectorAlgebraic<T>::operator+=(const VectorAlgebraic<T> &vectorAlgebraicT) {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        this->Array[i] += T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        this->Array[i] = T() + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        this->Array[j] = T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        this->Array[i] += vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        this->Array[j] += T();
                    }
                }
            } else {//Справа больше чем слева
                T *result = new T[vectorAlgebraicT.Size];
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] + T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result[i] = T() + vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] + vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = T();
                    }
                }
                this->Size = vectorAlgebraicT.Size;
                delete[] this->Array;
                this->Array = result;
            }
        } else {                                                                    //Векторы имеют один размер
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] += T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    this->Array[i] = T() + vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] += vectorAlgebraicT.Array[i];
                }
            }
        }
        this->Initialized = true;
        return *this;
    }

    template<class T>
    const VectorAlgebraic<T> &VectorAlgebraic<T>::operator-=(const VectorAlgebraic<T> &vectorAlgebraicT) {
        if (this->Size == 0 or vectorAlgebraicT.size() == 0) {
            throw std::domain_error("Vector(s) are degenerated");
        }
        if (!this->Initialized and !vectorAlgebraicT.Initialized) { //Оба не проинициализированны
            if (this->Size >= vectorAlgebraicT.Size) {
                return *this;
            } else {
                return vectorAlgebraicT;
            }
        } else if (this->size() != vectorAlgebraicT.size()) {            // Имеют разные размеры
            if (this->Size > vectorAlgebraicT.Size) {                        //Вектор слева больше
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        this->Array[i] -= T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        this->Array[i] = T() - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.Size; j < this->Size; ++j) {
                        this->Array[j] = -T();
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                        this->Array[i] -= vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = vectorAlgebraicT.size(); j < this->Size; ++j) {
                        this->Array[j] -= T();
                    }
                }
            } else {//Справа больше чем слева
                T *result = new T[vectorAlgebraicT.Size];
                if (this->Initialized and
                    !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] - T();
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = -T();
                    }
                } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                    for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {            //а слева нет
                        result[i] = T() - vectorAlgebraicT.Array[i];
                    }
                } else {                                                        //Оба вектора проинициализированны
                    for (size_t i = 0; i < this->Size; ++i) {
                        result[i] = this->Array[i] - vectorAlgebraicT.Array[i];
                    }
                    for (size_t j = this->Size; j < vectorAlgebraicT.Size; ++j) {
                        result[j] = -T();
                    }
                }
                this->Size = vectorAlgebraicT.Size;
                delete[] this->Array;
                this->Array = result;
            }
        } else {                                                                    //Векторы имеют один размер
            if (this->Initialized and !vectorAlgebraicT.Initialized) { //Вектор слева проинициализирован, а справа нет
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] -= T();
                }
            } else if (!this->Initialized and vectorAlgebraicT.Initialized) { // Вектор справа проинициализирован ,
                for (size_t i = 0; i < this->Size; ++i) {                                   //а слева нет
                    this->Array[i] = T() - vectorAlgebraicT.Array[i];
                }
            } else {                                                            //Оба вектора проинициализированны
                for (size_t i = 0; i < this->Size; ++i) {
                    this->Array[i] -= vectorAlgebraicT.Array[i];
                }
            }
        }
        this->Initialized = true;
        return *this;
    }

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator*(T number) const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic<T> temp(*this);
        if (!this->Initialized) {
            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] = T() * number;
            }
            temp.Initialized = true;
        } else {

            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] *= number;
            }
        }
        return temp;

    }

    template<class T>
    const VectorAlgebraic<T> VectorAlgebraic<T>::operator/(T number) const {
        if (this->Size == 0) {
            throw std::domain_error("Vector is degenerated");
        } else if (number == 0) {
            throw std::domain_error("Division by zero");
        }
        VectorAlgebraic<T> temp(*this);
        if (!this->Initialized) {
            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] = T() / number;
            }
            temp.Initialized = true;
        } else {

            for (size_t i = 0; i < this->Size; ++i) {
                temp.Array[i] /= number;
            }
        }
        return temp;
    }

    template<class T>
    const VectorAlgebraic<T> operator*(T number, const VectorAlgebraic<T> &vectorAlgebraicT) {
        if (vectorAlgebraicT.Size == 0) {
            throw std::domain_error("Vector is degenerated");
        }
        VectorAlgebraic<T> temp(vectorAlgebraicT);
        if (!vectorAlgebraicT.Initialized) {
            for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                temp.Array[i] = number * T();
            }
            temp.Initialized = true;
        } else {
            for (size_t i = 0; i < vectorAlgebraicT.Size; ++i) {
                temp.Array[i] = number * temp.Array[i];
            }
        }
        return temp;
    }


    template<class T>
    std::ostream &operator<<(std::ostream &os, const VectorAlgebraic<T> &vector) {
        size_t sz = vector.size();
        os << "\n{" << "\n\t\"name\":\"" << typeid(vector).name() << "\"" << "\n\t\"arraySize\": " << sz
           << "\n\t\"items\": [";
        for (size_t i = 0; i < sz; ++i) {
            os << "\n\t\t" << vector.items[i];
            if (i != sz - 1)os << ',';
        }
        os << "\n\t]\n}\n";
        return os;
    }

    template<class T>
    std::istream &operator>>(std::istream &is, VectorAlgebraic<T> &v) {
        std::string s;
        getline(is, s);
        VectorAlgebraic<T>::lineToVector(s, v);
        return is;
    }

    template<class T>
    void VectorAlgebraic<T>::lineToVector(std::string s, VectorAlgebraic<T> &v) {
        std::regex regexpr(R"((\d*\.?\d*)\s*\r*\n*)");
        std::cmatch result;
        std::regex_iterator<std::string::iterator> it(s.begin(), s.end(), regexpr);
        std::regex_iterator<std::string::iterator> end;
        delete[] v.items;
        long i1 = std::distance(it, end) - 1;
        v.items = new double[i1];
        for (size_t i = 0; it != end; ++it, ++i) {
            const auto &str = it->str();
            if (!str.empty())
                try {
                    v.items[i] = (std::stod(str));
                } catch (std::invalid_argument e) {
                    std::cerr << "Not a number";
                }
        }
        v.arraySize = static_cast<size_t>(i1);
    }
}


#endif //CPP_OOP_1_VECTORALGEBRAICT_H

