/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_OOP_1_VECTOR_H
#define CPP_OOP_1_VECTOR_H

#include <cstddef>
#include <iostream>
/**
 * @namespace cw
 */
namespace cw {
    /**
     * @author Alex Atamanenko
     * @brief Класс алгебраических векторов
     */
    class Vector {
    private:
        double *items;///Буфер

//        size_t buffered_size;///Размер буфера
        size_t size;///Размерность пространства вектора
        static size_t countObjects; ///Кол-во объектов класса
    public:
        /**
         * Конструктор создающий вырожденный вектор
         */
        Vector();

        /**
         * Копирующий конструктор
         * @param vector
         */
        Vector(const Vector &vector);

        /**
         * Создает вектор из массива
         * @param array
         * @param size_array - размер массива
         */
        Vector(const double *array, size_t size_array);

        /**
         * Создает вектор с размерностью пространства size и заполняет его значением defaultValue
         * @param size Размерность пространства
         * @param defaultValue
         */
        Vector(size_t size, double defaultValue);

        /**
         * Создает вектор с заданным размером буфера
         * @param buffered_size
         */
        explicit Vector(size_t buffered_size);

        virtual ~Vector();

        /**
         * Выводит json схему вектора
         * @param os
         * @param vector
         * @return std::ostream
         */
        friend std::ostream &operator<<(std::ostream &os, const Vector &vector);

        /**
         * Ввод вектора
         * @param is
         * @param v
         * @return std::istream
         */
        friend std::istream &operator>>(std::istream &is, Vector &v);

        /**
         * Сложение двух векторов
         * @param v
         * @return
         */
        const Vector operator+(const Vector &v) const;

        const Vector operator-(const Vector &v) const;

        const Vector &operator+() const;

        Vector operator-() const;

        Vector &operator++();

        const Vector operator++(int);

        double operator*(const Vector &v) const;

        const Vector operator*(double n) const;

        const Vector operator/(double n) const;

        bool operator==(const Vector &rhs) const;

        bool operator!=(const Vector &rhs) const;

        Vector &operator=(const Vector &vector);

        double &operator[](size_t index);

        const Vector &operator+=(const Vector &v);

        const Vector &operator-=(const Vector &v);

        const Vector &operator*=(double z);

        const Vector &operator/=(double z);

        const Vector &add(double item);

        explicit operator double *() const;

        const double len() const;

        const double angle(const Vector &&v) const;


        const bool isEmpty() const;

//        const Vector &clear();


//        static const Vector seq(size_t arraySize, ...);
        static size_t getCountObjects() {
            return countObjects;
        }

    private:
        friend void lineToVector(std::string s, Vector &v);

    };
}

#endif //CPP_OOP_1_VECTOR_H

#include "../../../../source/Vector.cpp"
