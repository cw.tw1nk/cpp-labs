/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CPP_OOP_1_VECTORT_H
#define CPP_OOP_1_VECTORT_H

#include <iostream>
#include <string>
#include <cstddef>
#include <cstring>

namespace cw {
    template<typename T>
    class TVector {
    protected:

        size_t arraySize;//< field for containing vectors' dimensions quantity
        T *items;
        bool isInit = false; //< filed to watch is this the zero-vector
    public:
        typedef size_t size_type;

        class iterator {
        public:
            typedef iterator self_type;
            typedef T value_type;
            typedef T &reference;
            typedef T *pointer;
            typedef std::random_access_iterator_tag iterator_category;
            typedef const size_type difference_type;

            explicit iterator(pointer ptr) : ptr_(ptr) {}

            self_type &operator++() {
                ++ptr_;
                return *this;
            }

            self_type &operator--() {
                --ptr_;
                return *this;
            }

            iterator &operator=(const self_type &other) {
                ptr_ = other.ptr;
                return *this;
            };

            const self_type operator++(int junk) {
                self_type i = *this;
                ++ptr_;
                return i;
            }

            const self_type operator--(int junk) {
                self_type i = *this;
                --ptr_;
                return i;
            }

            value_type &operator*() const { return *ptr_; }

            value_type *operator->() const { return ptr_; }

            const bool operator==(const self_type &rhs) const { return ptr_ == rhs.ptr_; }

            const bool operator!=(const self_type &rhs) const { return ptr_ != rhs.ptr_; }

            self_type operator+(const size_type &n) const { return iterator(ptr_ + n); }

            self_type operator-(const size_type &n) const { return iterator(ptr_ - n); }

            friend self_type operator+(const size_type &n, const iterator &it) { return iterator(it.ptr_ + n); }

            friend self_type operator-(const size_type &n, const iterator &it) { return iterator(it.ptr_ - n); }

            self_type &operator+=(const size_type &n) {
                ptr_ += n;
                return *this;
            }

            self_type &operator-=(const size_type &n) {
                ptr_ -= n;
                return *this;
            }

            const bool operator<(const self_type &it) const { return ptr_ < it.ptr_; }

            const bool operator>(const self_type &it) const { return ptr_ > it.ptr_; }

            const bool operator>=(const self_type &it) const { return ptr_ >= it.ptr_; }

            const bool operator<=(const self_type &it) const { return ptr_ <= it.ptr_; }

            value_type &operator[](const size_type &index) const { return *(ptr_ + index); }

        private:
            pointer ptr_;
        };

        class const_iterator {
        public:
            typedef const_iterator self_type;
            typedef const T value_type;
            typedef const T &reference;
            typedef const T *pointer;
            typedef const size_type difference_type;
            typedef std::random_access_iterator_tag iterator_category;

            explicit const_iterator(pointer ptr) : ptr_(ptr) {}

            const_iterator &operator=(const self_type &other) {
                ptr_ = other.ptr;
                return *this;
            };

            const self_type &operator++() {
                ptr_++;
                return *this;
            }

            const self_type &operator--() {
                --ptr_;
                return *this;
            }

            const self_type operator++(int junk) {
                self_type i = *this;
                ++ptr_;
                return i;
            }

            const self_type operator--(int junk) {
                self_type i = *this;
                --ptr_;
                return i;
            }

            const value_type &operator*() const { return *ptr_; }

            const value_type *operator->() const { return ptr_; }

            const bool operator==(const self_type &rhs) const { return ptr_ == rhs.ptr_; }

            const bool operator!=(const self_type &rhs) const { return ptr_ != rhs.ptr_; }

            const self_type operator+(const size_type &n) const { return iterator(ptr_ + n); }

            const self_type operator-(const size_type &n) const { return iterator(ptr_ - n); }

            friend const self_type operator+(const size_type &n, const self_type &it) { return iterator(it.ptr_ + n); }

            friend const self_type operator-(const size_type &n, const self_type &it) { return iterator(it.ptr_ - n); }

            const bool operator<(const self_type &it) const { return ptr_ < it.ptr_; }

            const bool operator>(const self_type &it) const { return ptr_ > it.ptr_; }

            const bool operator>=(const self_type &it) const { return ptr_ >= it.ptr_; }

            const bool operator<=(const self_type &it) const { return ptr_ <= it.ptr_; }

            const self_type &operator+=(const size_type &n) {
                ptr_ += n;
                return *this;
            }

            const self_type &operator-=(const size_type &n) {
                ptr_ -= n;
                return *this;
            }

            value_type &operator[](const size_type &index) const { return *(ptr_ + index); }

        private:
            pointer ptr_;
        };

        size_t size() const;

        static std::string ElementSeparator;
        static size_t County;

        TVector();

        explicit TVector(size_t size);

        TVector(size_t size, T number);

        TVector(const TVector<T> &vector);

        virtual ~TVector();

        TVector<T> &operator=(const TVector<T> &vector);

        const bool operator==(const TVector<T> &vector) const;

        const bool operator!=(const TVector<T> &vector) const;

        T &operator[](long long index) const;

        iterator begin();

        iterator end();

        const_iterator cbegin() const;

        const_iterator cend() const;

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const TVector<V> &vectorT);

        template<class V>
        friend std::istream &operator>>(std::istream &is, TVector<V> &vectorT);

        void *operator new[](size_t lenght);

        void operator delete[](void *p);


    };

    template<class T>
    size_t TVector<T>::County;

    template<class T>
    size_t TVector<T>::size() const {
        return arraySize;
    }

    template<class T>
    TVector<T>::TVector() {
        ++County;
        arraySize = 0;
        items = nullptr;
    }

    template<class T>
    TVector<T>::TVector(size_t size): arraySize(size) {
        ++County;
        items = new T[size];
        if (items == nullptr) {
            throw std::bad_alloc();
        }
    }

    template<class T>
    TVector<T>::TVector(size_t size, T number): arraySize(size) {
        isInit = true;
        ++County;
        items = new T[size];
        if (items == nullptr) {
            throw std::bad_alloc();
        }
        for (size_t i = 0; i < size; ++i) {
            items[i] = number;
        }
    }

    template<class T>
    TVector<T>::TVector(const TVector<T> &vector) {
        isInit = vector.isInit;
        ++County;
        if (vector.arraySize() != 0) {
            items = new T[arraySize = vector.arraySize()];
            if (isInit) {
                for (size_t i = 0; i < arraySize; ++i) {
                    items[i] = vector.items[i];
                }
            }
        } else {
            items = nullptr;
            arraySize = 0;
        }
    }

    template<class T>
    TVector<T>::~TVector() {
        --County;
        delete[] items;
    }

    template<class T>
    TVector<T> &TVector<T>::operator=(const TVector<T> &vector) {
        if (vector.isInit) {
            isInit = true;
        } else if (this->isInit and !vector.isInit) {
            isInit = false;
        }
        if (this->arraySize() != 0 and vector.arraySize() != 0) {
            if (arraySize != vector.arraySize()) {
                delete[] items;
                items = new T[arraySize = vector.arraySize()];
            }
            if (vector.isInit) {
                for (size_t i = 0; i < arraySize; ++i) {
                    items[i] = vector.items[i];
                }
            }
        } else if (arraySize == 0 and vector.arraySize() != 0) {
            items = new T[arraySize = vector.arraySize()];
            if (this->isInit) {
                for (size_t i = 0; i < arraySize; ++i) {
                    items[i] = vector.items[i];
                }
            }
        } else if (arraySize != 0 && vector.arraySize() == 0) {
            delete[] items;
        }
        return *this;
    }

    template<class T>
    const bool TVector<T>::operator==(const TVector<T> &vector) const {
        if (!this->isInit and !vector.isInit) {
            return true;
        }
        if (this->isInit and !vector.isInit) {
            for (size_t i = 0; i < arraySize; ++i) {
                if (items[i] != T()) {
                    return false;
                }
            }
        } else if (vector.isInit and !this->isInit) {
            for (size_t i = 0; i < arraySize; ++i) {
                if (items[i] != T()) {
                    return true;
                }
            }
        } else {
            if (arraySize != vector.arraySize) {
                size_t temp;
                (arraySize > vector.arraySize) ? temp = arraySize : temp = vector.arraySize;
                for (size_t i = 0; i < temp - 1; ++i) {
                    if (items[i] != T() or vector.items[i] != T()) {
                        return false;
                    }
                }
                if (temp == arraySize) return items[temp] == T();
                else return vector.items[temp] == T();
            } else {
                for (size_t i = 0; i < arraySize; ++i) {
                    if (items[i] != vector.items[i]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    template<class T>
    const bool TVector<T>::operator!=(const TVector<T> &vector) const {
        if (!this->isInit and !vector.isInit) {
            return false;
        }
        if (this->isInit and !vector.isInit) {
            for (size_t i = 0; i < arraySize; ++i) {
                if (items[i] != T()) {
                    return true;
                }
            }
        } else if (vector.isInit and !this->isInit) {
            for (size_t i = 0; i < arraySize; ++i) {
                if (items[i] != T()) {
                    return false;
                }
            }
        } else {
            if (arraySize != vector.arraySize) {
                size_t temp;
                (arraySize > vector.arraySize) ? temp = arraySize : temp = vector.arraySize;
                for (size_t i = 0; i < temp - 1; ++i) {
                    if (items[i] != T() or vector.items[i] != T()) {
                        return true;
                    }
                }
                if (temp == arraySize) return items[temp] != T();
                else return vector.items[temp] != T();
            } else {
                for (size_t i = 0; i < arraySize; ++i) {
                    if (items[i] == vector.items[i]) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    template<class T>
    T &TVector<T>::operator[](long long index) const {
        if (index < 0) {
            throw std::out_of_range("Index is out of range");
        }
        auto tempIndex = (TVector<T>::size_type) index;
        if (arraySize == 0) {
            throw std::domain_error("Degenerated vectors don't have coordinates");
        } else if (tempIndex >= arraySize) {
            throw std::out_of_range("Index is out of range");
        }
        if (this->isInit) {
            return items[index];
        } else {
            return (items[index] = T());
        }
    }


    template<class T>
    std::ostream &operator<<(std::ostream &os, const TVector<T> &vector) {
        size_t sz = vector.size();
        os << "\n{" << "\n\t\"name\":\"" << typeid(vector).name() << "\"" << "\n\t\"arraySize\": " << sz
           << "\n\t\"items\": [";
        for (size_t i = 0; i < sz; ++i) {
            os << "\n\t\t" << vector.items[i];
            if (i != sz - 1)os << ',';
        }
        os << "\n\t]\n}\n";
        return os;
    }


    template<class T>
    std::istream &operator>>(std::istream &is, TVector<T> &vector) {
        for (size_t i = 0; i < vector.arraySize; ++i) {
            is >> vector.items[i];
        }
        return is;
    }

    template<typename T>
    void *TVector<T>::operator new[](size_t lenght) {
        return malloc(lenght * sizeof(TVector<T>));
    }


    template<class T>
    typename TVector<T>::iterator TVector<T>::begin() {
        return iterator(this->items);
    }

    template<class T>
    typename TVector<T>::iterator TVector<T>::end() {
        return iterator(this->items + arraySize);
    }

    template<class T>
    typename TVector<T>::const_iterator TVector<T>::cbegin() const {
        return const_iterator(this->items);
    }

    template<class T>
    typename TVector<T>::const_iterator TVector<T>::cend() const {
        return const_iterator(this->items + arraySize);
    }

    template<typename T>
    void TVector<T>::operator delete[](void *p) {
        free(p);
    }

}
#endif //CPP_OOP_1_VECTORT_H

