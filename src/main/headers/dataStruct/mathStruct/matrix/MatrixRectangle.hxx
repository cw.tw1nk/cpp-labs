/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_MATRIXALGEBRAIC_H
#define CPP_OOP_MATRIXALGEBRAIC_H

#include <sstream>
#include <cmath>
#include <cstring>
#include <iostream>
#include <cstddef>
#include <stdexcept>
#include <dataStruct/mathStruct/matrix/Matrix.hxx>
#include <dataStruct/mathStruct/vector/VectorAlgebraic.hxx>


/**
 * @brief Class template Algebraic Matrices
 * @author AnthonyJ , Alex Atamanenko
 * @tparam T Any type with arithmetic operations
 */
namespace cw {
    template<class T = double>
    class MatrixRectangle : public Matrix<T> {

        static constexpr long double EPSILON = 1e-14; // comparator error
    public:

        MatrixRectangle();

        MatrixRectangle(const T **t, size_t rows, size_t cols);

        MatrixRectangle(size_t rows, size_t columns);

        MatrixRectangle(size_t rows, size_t columns, T number);

        MatrixRectangle(const MatrixRectangle<T> &other);

        virtual ~MatrixRectangle() = default;

        virtual MatrixRectangle<T> &operator=(const MatrixRectangle<T> &other);

        const MatrixRectangle<T> operator+(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> operator-(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> operator*(const MatrixRectangle<T> &other) const;


        const MatrixRectangle<T> operator*(const T &number) const;

        const MatrixRectangle<T> operator/(const T &number) const;

        const MatrixRectangle<T> &operator*=(const T &number) const;

        const MatrixRectangle<T> &operator/=(const T &number) const;

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const MatrixRectangle<V> &t);

        template<class V>
        friend const MatrixRectangle<V> operator*(const V &number, const MatrixRectangle<V> &matrix);

        template<class V>
        friend const MatrixRectangle<V> operator/(const V &number, const MatrixRectangle<V> &matrix);

        const MatrixRectangle<T> &operator+=(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> &operator-=(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> &operator*=(const MatrixRectangle<T> &other);


        virtual const MatrixRectangle<T> getMinor(size_t RowNumber, size_t ColumnNumber) const;


        const bool operator==(const MatrixRectangle<T> &other) const;

        const bool operator!=(const MatrixRectangle<T> &other) const;

        const MatrixRectangle<T> transpose() const;

        const VectorAlgebraic<T> operator*(const VectorAlgebraic<T> &vectorAlgebraic) const;

        MatrixRectangle(const VectorAlgebraic<T> &vector);

        const bool operator==(const Matrix<T> &matrixT) const {
            if (this->rows != matrixT.rows or this->columns != matrixT.columns) {
                return false;
            } else {
                for (size_t i = 0; i < this->rows; ++i) {
                    for (size_t j = 0; j < this->columns; ++j) {
                        if (this->array[i][j] != matrixT.array[i][j]) {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        const bool operator!=(const Matrix<T> &matrixT) const {
            return !(*this == matrixT);
        }
    };


    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator+(const MatrixRectangle<T> &other) const {
        if (this->rows * other.rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->rows != other.rows or this->columns != other.columns) {
            throw std::domain_error("Matrices are not compatible");
        }
        MatrixRectangle<T> result(this->rows, this->columns);
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result(i, j) = this->array[i][j] + other.array[i][j];
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator-(const MatrixRectangle<T> &other) const {
        if (this->rows * other.rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->rows != other.rows or this->columns != other.columns) {
            throw std::domain_error("Matrices are not compatible");
        }
        MatrixRectangle<T> result(this->rows, this->columns);
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result(i, j) = this->array[i][j] - other.array[i][j];
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator*(const T &number) const {
        if (this->rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        }
        MatrixRectangle<T> result(this->rows, this->columns);
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result(i, j) = this->array[i][j] * number;
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator/(const T &number) const {
        if (this->rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        } else if (number == T()) {
            throw std::domain_error("Division by zero");
        }
        MatrixRectangle<T> result(this->rows, this->columns);
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result.array[i][j] = this->array[i][j] / number;
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> operator*(const T &number, const MatrixRectangle<T> &matrix) {
        if (matrix.rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        }
        MatrixRectangle<T> result(matrix.rows, matrix.columns);
        for (size_t i = 0; i < matrix.rows; ++i) {
            for (size_t j = 0; j < matrix.columns; ++j) {
                result(i, j) = matrix.array[i][j] / number;
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator*=(const T &number) const {

        if (this->rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        }
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                this->array[i][j] *= number;
            }
        }
        return *this;
    }

    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator/=(const T &number) const {
        if (this->rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        } else if (number == T()) {
            throw std::domain_error("Division by zero");
        }
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                this->array[i][j] /= number;
            }
        }
        return *this;
    }

    template<class T>
    const MatrixRectangle<T> operator/(const T &number, const MatrixRectangle<T> &matrix) {
        if (matrix.rows == 0) {
            throw std::domain_error("The Matrix is degenerated");
        } else if (number == T()) {
            throw std::domain_error("Division by zero");
        }
        MatrixRectangle<T> result(matrix.rows, matrix.columns);
        for (size_t i = 0; i < matrix.rows; ++i) {
            for (size_t j = 0; j < matrix.columns; ++j) {
                result.array[i][j] = matrix.array[i][j] / number;
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator+=(const MatrixRectangle<T> &other) const {
        if (this->rows * other.rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->rows != other.rows or this->columns != other.columns) {
            throw std::domain_error("Matrices are not compatible");
        }

        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                this->array[i][j] += other.array[i][j];
            }
        }
        return *this;
    }

    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator-=(const MatrixRectangle<T> &other) const {
        if (this->rows * other.rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        } else if (this->rows != other.rows or this->columns != other.columns) {
            throw std::domain_error("Matrices are not compatible");
        }
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                this->array[i][j] -= other.array[i][j];
            }
        }
        return *this;
    }

    template<class V>
    std::ostream &operator<<(std::ostream &os, const MatrixRectangle<V> &t) {
        os << "\n{" <<
           "\n\t\"name\":\"" << typeid(t).name() << "\"" <<
           "\n\t\"rows\": " << t.rows <<
           "\n\t\"columns\": " << t.columns <<
           "\n\t\"items\": [";
        for (size_t i = 0; i < t.rows; ++i) {
            os << "\n\t\t[\n\t\t\t";
            for (size_t j = 0; j < t.columns; ++j) {
                os << t.array[i][j];
                if (j != t.columns - 1)os << ", ";
            }
            os << "\n\t\t]";
            if (i != t.rows - 1)os << ',';
        }
        os << "\n\t]\n}\n";
        return os;
    }

    template<class T>
    MatrixRectangle<T> &MatrixRectangle<T>::operator=(const MatrixRectangle<T> &other) {
        if (*this == other) {
            return *this;
        } else if (this->rows == other.rows and this->columns == other.columns) {
            memcpy(this->array[0], other.array[0], sizeof(T) * this->rows * this->columns);
            for (size_t i = 1; i < this->rows; ++i) {
                *(this->array + i) = *(this->array) + this->columns * i;
            }
        } else {
            delete[] this->array;
            this->rows = other.rows;
            this->columns = other.columns;
            try {
                this->array = Matrix<T>::createArray2d(this->rows, this->columns);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            memcpy(this->array[0], other.array[0], sizeof(T) * this->rows * this->columns);
            for (size_t i = 1; i < this->rows; ++i) {
                *(this->array + i) = *(this->array) + this->columns * i;
            }
        }
        return *this;
    }

    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::operator*(const MatrixRectangle<T> &other) const {
        if (this->columns != other.rows) {
            throw std::domain_error("Matrices are not compatible");
        } else if (this->rows * other.rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        }
        MatrixRectangle<T> result(this->rows, other.columns);
        T sum = 0;
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < other.columns; ++j) {
                sum = 0;
                for (size_t k = 0; k < this->columns; ++k) {
                    sum += this->array[i][k] * other.array[k][j];
                }
                result.array[i][j] = sum;
            }
        }
        return result;
    }

    template<class T>
    const MatrixRectangle<T> &MatrixRectangle<T>::operator*=(const MatrixRectangle<T> &other) {
        if (this->columns != other.rows) {
            throw std::domain_error("Matrices are not compatible");
        } else if (this->rows * other.rows == 0) {
            throw std::domain_error("Matrix is degenerated");
        }
        T **result;
        try {
            result = MatrixRectangle<T>::createArray2d(this->rows, other.columns);
        } catch (std::bad_alloc &e) {
            throw e;
        }
        T sum = 0;
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < other.columns; ++j) {
                sum = 0;
                for (size_t k = 0; k < this->columns; ++k) {
                    sum += this->array[i][k] * other.array[k][j];
                }
                result[i][j] = sum;
            }
        }
        delete[] this->array;
        this->array = result;
        return *this;
    }


    template<class T>
    MatrixRectangle<T>::MatrixRectangle(): Matrix<T>() {

    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(size_t
                                        rows, size_t
                                        columns): Matrix<T>(rows, columns) {

    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(size_t
                                        rows, size_t
                                        columns, T
                                        number):Matrix<T>(rows, columns, number) {

    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(const MatrixRectangle<T> &other) : Matrix<T>(other) {

    }


    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::getMinor(size_t
                                                          RowNumber, size_t
                                                          ColumnNumber) const {
        if (this->rows == 0) {
            throw std::domain_error("Degenerated matrices does not have minors");
        } else if (this->rows == 1 or this->columns == 1) {
            throw std::domain_error("Matrix does not have minors(Size is to small)");
        }
        MatrixRectangle<T> result(this->rows - 1, this->columns - 1);
        for (size_t i = 0, k = 0; k < this->rows - 1; ++i, ++k) {
            if (i == RowNumber) ++i;
            for (size_t j = 0, l = 0; l < this->columns - 1; ++j, ++l) {
                if (j == ColumnNumber) ++j;
                result.array[k][l] = this->array[i][j];
            }
        }
        return result;
    }


    template<class T>
    const MatrixRectangle<T> MatrixRectangle<T>::transpose() const {
        if (this->rows * this->columns == 0) {
            throw std::domain_error("Degenerated matrices does not have transposed matrix");
        } else if (this->rows == 1 and this->columns == 1) {
            return *this;
        }
        MatrixRectangle<T> result(this->rows, this->columns);
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result.array[i][j] = this->array[j][i];
            }
        }
        return result;
    }

    template<class T>
    const bool MatrixRectangle<T>::operator==(const MatrixRectangle<T> &other) const {
        if (this->rows != other.rows or this->columns != other.columns) {
            return false;
        } else {
            if (this->rows * other.rows * this->columns * other.columns == 0) {
                return false;
            }
            for (size_t i = 0; i < this->rows; ++i) {
                for (size_t j = 0; j < this->columns; ++j) {
                    if (fabs(this->array[i][j] - other.array[i][j]) > EPSILON) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    template<class T>
    const bool MatrixRectangle<T>::operator!=(const MatrixRectangle<T> &other) const {
        if (this->rows != other.rows or this->columns != other.columns) {
            return true;
        } else {
            if (this->rows * other.rows * this->columns * other.columns == 0) {
                return true;
            }
            for (size_t i = 0; i < this->rows; ++i) {
                for (size_t j = 0; j < this->columns; ++j) {
                    if (fabs(this->array[i][j] - other.array[i][j]) < EPSILON) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    template<class T>
    const VectorAlgebraic<T> MatrixRectangle<T>::operator*(const VectorAlgebraic<T> &vectorAlgebraic) const {
        if (this->columns != vectorAlgebraic.size()) {
            throw std::domain_error("The Matrix and the vector are not compatible");
        } else {
            VectorAlgebraic<T> result(vectorAlgebraic.size());
            T sum = 0;
            for (size_t i = 0; i < this->rows; ++i) {
                for (size_t j = 0; j < vectorAlgebraic.size(); ++j) {
                    sum = 0;
                    for (size_t k = 0; k < vectorAlgebraic.size(); ++k) {
                        sum += this->array[i][k] * vectorAlgebraic[k];
                    }
                    result[i] = sum;
                }
            }
            return result;
        }
    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(const VectorAlgebraic<T> &vectorAlgebraic) {
        if (vectorAlgebraic.size() == 0) {
            this->rows = this->columns = 0;
            this->array = nullptr;
        } else {
            this->rows = vectorAlgebraic.size();
            this->columns = 1;
            this->array = this->createArray2d(this->rows, this->columns);
            memccpy(this->array[0], vectorAlgebraic.Array, this->rows * sizeof(T));
        }
    }

    template<class T>
    MatrixRectangle<T>::MatrixRectangle(const T **t, size_t rows, size_t cols): Matrix<T>(t, rows, cols) {}

}
#endif //CPP_OOP_MATRIXALGEBRAIC_H

