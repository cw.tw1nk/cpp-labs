/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_LIST_HXX
#define CPP_OOP_1_LIST_HXX

#include <DeNode.hxx>

template<class T>
class List {
private:
    DeNode <T> *head;
    DeNode <T> *tail;
    DeNode <T> *current;

    size_t size;

    List() : head(nullptr), tail(nullptr), current(nullptr) {}

    List(const List<T> &other) : size(other.size) {
        auto current = other.head;
        for (int i = 0; i < size; ++i) {
            add(current->get());
            current = current->getNext();
        }
    }

    List &add(const T &item) {
        auto node = new DeNode<T>(item);
        if (size == 0) {
            head = node;
            tail = node;
            current = node;
            head->setNext(tail);
            head->setPrevious(tail);
            tail->setNext(head);
            tail->setPrevious(head);
        } else {
            tail->setNext(node);
            node->setPrevious(tail);
            tail = node;
            tail->setNext(head);
            head->setPrivous(tail);
        }
        return *this;
    }

};


#endif //CPP_OOP_1_LIST_HXX
