#pragma once

#include <ctime>

namespace lcg {
// ������� �������� 2^64: x64 = 18446744073709551616;
    static size_t a = static_cast<int>(6364136223846793005);
    static size_t c = static_cast<int>(1442695040888963407);
    static size_t x = static_cast<int>(1442695040888963407);
    static size_t m = static_cast<int>(4294967295);

    // ������� ���������� ��������� ����� ����� �� ��������� 0..4294967295 (����������� �������������)
    size_t rand32() {
        lcg::x = lcg::a * lcg::x + lcg::c;
        return lcg::x;
    }

    // ������� ���������� ����������� ��������� ����� �����, ������������ �������� rand32
    size_t rand32_max() { return m; }

    size_t rand32(int max) {

        return rand32() % max;

    }

    // ������� ������������� ��������� �������� ���������� ��������� �����
    void randomize() { lcg::x = size_t(clock()); }
}
