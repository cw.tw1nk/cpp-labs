//#pragma once
//#include<windows.h>
//
//class HRTimer {
//public:
//	HRTimer() { frequency = 1.0 / GetFrequency(); };
//
//	// Функция возвращает период сигналов таймера (в секундах)
//	double GetFrequency() {
//		LARGE_INTEGER proc_freq;
//		::QueryPerformanceFrequency(&proc_freq);
//		return static_cast<double>(proc_freq.QuadPart);
//	}
//
//	// Функция, запускающая таймер
//	void StartTimer() {
//		// Установка потока для использования процессора 0
//		DWORD_PTR oldmask = ::SetThreadAffinityMask(::GetCurrentThread(), 0);
//		::QueryPerformanceCounter(&start);
//		// Восстановить исходный
//		::SetThreadAffinityMask(::GetCurrentThread(), oldmask);
//	};
//
//	// Функция останавливает таймер и возвращает время его работы
//	// (т.е. время, прошедшее с момента последнего вызова функции void StartTimer()
//	double StopTimer() {
//		// Установка потока для использования процессора 0
//		DWORD_PTR oldmask = ::SetThreadAffinityMask(::GetCurrentThread(), 0);
//		::QueryPerformanceCounter(&stop);
//		// Восстановить исходный
//		::SetThreadAffinityMask(::GetCurrentThread(), oldmask);
//		return ((stop.QuadPart - start.QuadPart) * frequency);
//	};
//
//private:
//	LARGE_INTEGER start; // время запуска
//	LARGE_INTEGER stop;  // время остановки
//	double frequency;    // период сигналов таймера (в секундах)
//};
