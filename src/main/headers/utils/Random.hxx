/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_RANDOM_HXX
#define CPP_OOP_1_RANDOM_HXX

#include <random>
#include <iostream>
#include <functional>
#include <limits>
#include <cstddef>
#include <chrono>
#include <utils/Timer.hxx>

using std::chrono::time_point;
using std::chrono::high_resolution_clock;
using std::chrono::nanoseconds;
using std::chrono::duration_cast;

namespace cw::random {
    template<typename T>
    T rand() {
        std::random_device rd;
        std::uniform_int_distribution<T> uid(0, std::numeric_limits<T>::max());
        return uid(rd);
    }

    template<typename T>
    T rand(T max) {
        std::random_device rd;
        std::uniform_int_distribution<T> uid(0, max);
        return uid(rd);
    }

    template<typename T>
    T rand(T min, T max) {
        std::random_device rd;
        std::uniform_int_distribution<T> uid(min, max);
        return uid(rd);
    }

    template<typename T>
    T crand(T min, T max) {
        std::random_device::result_type sd = std::random_device()();
        return crand(min, max, sd);
    }

    template<typename T>
    T chrtrand(T min, T max) {
        std::mt19937 gen(cw::rdtsc());
        return std::uniform_int_distribution<T>(min, max)(gen);
    }

    template<typename T>
    T crand(T min, T max, std::random_device::result_type &sd) {
        std::mt19937 gen(sd);
        return std::uniform_int_distribution<T>(min, max)(gen);
    }

    std::random_device::result_type genSeed() {
        return std::random_device()();
    }

}
namespace cw::random::lcg {
    size_t next = 0x14057B7EF767814F;
    size_t a = 0x5851F42D4C957F2D;
    size_t m = 0xffffffffffffffff;
    size_t c = 0x14057B7EF767814F;

    inline size_t rand() {
        return next = next * a + c;
    }

    inline size_t rand(size_t min, size_t max) {
        size_t range = max - min + 1;
        return rand() % ((range) + min);
    }

    inline void generateSeed() {
        cw::random::lcg::next = cw::rdtsc();
    }

    inline void seed(size_t seed) {
        cw::random::lcg::next = seed;
    }

}
#endif //CPP_OOP_1_RANDOM_HXX
