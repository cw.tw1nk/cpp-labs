/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string>
#include <benchmark/benchmark.h>
#include <dataStruct/TArray.hxx>
#include <dataStruct/Stack.hxx>
#include <alg/Alg.h>

static void BM_Stack(benchmark::State &state) {
    cw::Stack<int> stack;
    for (auto _:state) {
        for (int i = 0; i < state.range(0); ++i) {
            stack.push(100);
        }
        for (int i = 0; i < state.range(0); ++i) {
            stack.pop();
        }
    }
}

BENCHMARK(BM_Stack)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 15);

static void BM_Bubble_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::bubbleSort(tArray, false);
    }
}

BENCHMARK(BM_Bubble_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 14);

static void BM_Insertion_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::insertionSort(tArray, false);
    }
}

BENCHMARK(BM_Insertion_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 14);

static void BM_Selection_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::selectionSort(tArray, false);
    }
}

BENCHMARK(BM_Selection_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 14);

static void BM_Quick_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::quickSort(tArray, 0, static_cast<int>(tArray.size() - 1), cw::sort::helper::compare<int>);
    }
}

BENCHMARK(BM_Quick_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 22);

static void BM_Hybrid_Quick_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::hybridQuickSort(tArray, 0, static_cast<int>(tArray.size() - 1), cw::sort::helper::compare<int>);
    }
}

BENCHMARK(BM_Hybrid_Quick_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 22);

static void BM_Stack_Quick_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::stackQuickSort(tArray, 0, static_cast<int>(tArray.size() - 1), cw::sort::helper::compare<int>);
    }
}

BENCHMARK(BM_Stack_Quick_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 22);

static void BM_Parallel_Quick_sort(benchmark::State &state) {
    for (auto _:state) {
        state.PauseTiming();
        const cw::TArray<int> &tArray = cw::TArray<int>::seq(static_cast<size_t>(state.range(0)));
        tArray.shuffle();
        state.ResumeTiming();
        cw::sort::parallelQuickSort(tArray, 0, static_cast<int>(tArray.size() - 1), cw::sort::helper::compare<int>);
    }
}

BENCHMARK(BM_Parallel_Quick_sort)->Unit(benchmark::kNanosecond)->RangeMultiplier(2)->Range(1 << 10, 1 << 22);

BENCHMARK_MAIN();