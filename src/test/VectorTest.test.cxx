/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <dataStruct/mathStruct/vector/Vector.h>

using cw::Vector;

TEST(Vector, equality) {
    double a[5] = {0, 1, 2, 3, 4};
    double d[4] = {0, 1, 2, 3};
    double b[4] = {1, 2, 3, 4};
//    Vector v({1,2,3},4);
    ASSERT_TRUE(Vector(a, 5) == Vector(a, 5));
    ASSERT_FALSE(Vector(a, 5) == Vector(b, 4));
    ASSERT_FALSE(Vector(b, 4) == Vector(d, 4));
    ASSERT_TRUE(Vector(a, 5) != Vector(b, 4));
    ASSERT_FALSE(Vector(a, 5) != Vector(a, 5));
}

TEST(Vector, addition) {
    double a[5] = {1, 1, 1, 1, 2};
    double b[5] = {2, 2, 2, 2, 4};
    double d[6] = {1, 1, 1, 2, 1, 1};
    double e[6] = {2, 2, 2, 3, 3, 1};
    ASSERT_EQ(Vector(a, 5) + Vector(a, 5), Vector(b, 5));
    ASSERT_EQ(Vector(a, 5) + Vector(d, 6), Vector(e, 6));
    ASSERT_EQ(Vector(a, 5) + Vector(), Vector(a, 5));
}

TEST(Vector, subtraction) {
    double a[5] = {1, 1, 1, 1, 2};
    double b[5] = {0, 0, 0, 0, 0};
    double d[6] = {1, 1, 1, 2, 1, 1};
    double e[6] = {0, 0, 0, -1, 1, -1};
    ASSERT_EQ(Vector(a, 5) - Vector(a, 5), Vector(b, 5));
    ASSERT_EQ(Vector(a, 5) - Vector(d, 6), Vector(e, 6));
    ASSERT_EQ(Vector(a, 5) - Vector(), Vector(a, 5));
}

TEST(Vector, multiplication) {
    double a[5] = {1, 1, 1, 1, 2};
    double b[5] = {2, 2, 2, 2, 4};
    double c[6] = {2, 1, 1, 1, 1, 1};
    ASSERT_EQ(Vector(a, 5) * Vector(a, 5), 8);
    ASSERT_EQ(Vector(a, 5) * 2, Vector(b, 5));
    ASSERT_EQ(Vector(a, 5) * Vector(c, 6), 7);
}

TEST(Vector, division) {
    double a[5] = {1, 1, 1, 1, 2};
    double b[5] = {1. / 2, 1. / 2, 1. / 2, 1. / 2, 1};
    ASSERT_EQ(Vector(a, 5) / 2, Vector(b, 5));
}

