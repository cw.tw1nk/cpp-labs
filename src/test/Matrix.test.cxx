/*
 *    Copyright © 2019 Alex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
//#include <mathStruct/matrix/Matrix.hxx>
//#include <mathStruct/matrix/MatrixRectangle.hxx>
#include <dataStruct/mathStruct/matrix/MatrixSquare.hxx>

using cw::Matrix;
using cw::MatrixRectangle;
using cw::MatrixSquare;

TEST(Matrix, equals) {
    Matrix a(2, 2, 0);
    Matrix b(2, 2, 0);
    Matrix c(2, 2, 1);
    ASSERT_TRUE(a == b);
    ASSERT_FALSE(a == c);
    ASSERT_TRUE(a != c);
}

TEST(MatrixRectangle, addition) {
    ASSERT_EQ(MatrixRectangle(3, 3, 2) + MatrixRectangle(3, 3, 4), MatrixRectangle(3, 3, 6));
    ASSERT_EQ(MatrixRectangle(3, 3, 2) += MatrixRectangle(3, 3, 4), MatrixRectangle(3, 3, 6));
}

TEST(MatrixRectangle, subtraction) {
    ASSERT_EQ(MatrixRectangle(3, 3, 2) + MatrixRectangle(3, 3, 4), MatrixRectangle(3, 3, 6));
    ASSERT_EQ(MatrixRectangle(3, 3, 2) += MatrixRectangle(3, 3, 4), MatrixRectangle(3, 3, 6));
}

TEST(MatrixRectangle, multiplication) {
    ASSERT_EQ(MatrixRectangle(3, 3, 2) * MatrixRectangle(3, 3, 4), MatrixRectangle(3, 3, 24));
    ASSERT_EQ(MatrixRectangle(3, 3, 2) *= MatrixRectangle(3, 3, 4), MatrixRectangle(3, 3, 24));
    ASSERT_EQ(MatrixRectangle(3, 3, 2) * 2, MatrixRectangle(3, 3, 4));
    ASSERT_EQ(MatrixRectangle(3, 3, 2) *= 2, MatrixRectangle(3, 3, 4));
}

TEST(MatrixRectangle, devision) {
    ASSERT_EQ(MatrixRectangle(3, 3, 2) / 2, MatrixRectangle(3, 3, 1));
}

TEST(MatrixSquare, determinant) {
    MatrixSquare sq0(2, 2);
    sq0(0, 0) = 1;
    sq0(0, 1) = 1;
    sq0(1, 0) = 1;
    sq0(1, 1) = 1;
    ASSERT_EQ(sq0.determinant(), 0);
    MatrixSquare sq1(2, 2);
    sq1(0, 0) = 1;
    sq1(0, 1) = 8;
    sq1(1, 0) = 9;
    sq1(1, 1) = 3;
    ASSERT_EQ(sq1.determinant(), -69);
}
